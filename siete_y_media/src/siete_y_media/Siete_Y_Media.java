/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package siete_y_media;

import baraja.Naipe;
import baraja.Baraja;


/**
 *
 * @author alumno
 */
public class Siete_Y_Media {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        //crear la baraja
        Baraja b=new Baraja(false);//sin 8 y 9
        //poner valor a cada naipe
        Naipe n;
        for(int palo=Baraja.OROS;palo<=Baraja.BASTOS;palo++){
            for(int numero=1;numero<=10;numero++){
                n=b.getNaipe(palo, numero);
                if(numero>7){//SOTA, CABALLO Y REY
                    n.setValor(0.5f);
                }else{
                    n.setValor(numero);
                }
            }
        }
        //System.out.println(b);
        
        //JUEGA EL JUGADOR
        System.out.println("JUEGAS TU");
        System.out.println("---------");
        float puntosJ=0;
        char sino=' ';
        do{
          //dar carta al jugador
          n=b.repartir();
          System.out.println(n);
          //sumar los puntos
          puntosJ=puntosJ+n.getValor();
            System.out.println("Tu puntuación hasta el momento es "+puntosJ);
          //si no se ha pasado y no tiene 7,5 preguntamos si desea continuar
          if(puntosJ<7.5){
              System.out.print("¿Desea continuar (s/n)?");
              sino=Character.toLowerCase(cs1.Keyboard.readChar());
          }
        }while(puntosJ<7.5 && sino=='s');

        if(puntosJ<=7.5){
            //JUEGA LA BANCA
            float puntosB=0;
            do{
                //sacar una carta del mazo
                n=b.repartir();
                System.out.println(n);
                //sumar los puntos
                puntosB=puntosB+n.getValor();
            }while(puntosB<puntosJ && puntosB<7.5);
            //¿quien ha ganado?
            if(puntosB<=7.5){
                System.out.println("Lo siento has perdido");
            }else{
                System.out.println("¡Enhorabuena! Has ganado");
            }
        }else{
            System.out.println("Te has pasado. Gana la banca.");
        }
    }
    
}
