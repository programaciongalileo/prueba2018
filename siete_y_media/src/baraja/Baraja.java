/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baraja;

/**
 *
 * @author alumno
 */
public class Baraja {

    //Contantes
    public static final int OROS = 0;
    public static final int COPAS = 1;
    public static final int ESPADAS = 2;
    public static final int BASTOS = 3;

    public static final int AS = 1;
    public static final int DOS = 2;
    public static final int TRES = 3;
    public static final int CUATRO = 4;
    public static final int CINCO = 5;
    public static final int SEIS = 6;
    public static final int SIETE = 7;
    public static final int OCHO = 8;
    public static final int NUEVE = 9;
    public static final int SOTA = 10;
    public static final int CABALLO = 11;
    public static final int REY = 12;

    //Atributos
    private final Naipe[][] baraja;
    private final boolean con8y9;
    private int mazo;

    //Constructor
    public Baraja(boolean con8y9) {
        if (con8y9 == false) {
            this.baraja = new Naipe[4][10];
            this.mazo = 40;
        } else {
            this.baraja = new Naipe[4][12];
            this.mazo = 48;
        }
        this.con8y9 = con8y9;
        crearNaipes();
    }

    //Métodos
    private void crearNaipes() {
        for (int p = OROS; p <= BASTOS; p++) {
            for (int n = 0; n < mazo / 4; n++) {
                int numero = n + 1;
                switch (numero) {
                    case AS:
                    case DOS:
                    case TRES:
                    case CUATRO:
                    case CINCO:
                    case SEIS:
                    case SIETE:
                        baraja[p][n] = new Naipe(p, numero, 0);
                        break;
                    case OCHO:
                        if (con8y9 == true) {
                            baraja[p][n] = new Naipe(p, Baraja.OCHO, 0);
                        } else {
                            baraja[p][n] = new Naipe(p, Baraja.SOTA, 0);
                        }
                        break;
                    case NUEVE:
                        if (con8y9 == true) {
                            baraja[p][n] = new Naipe(p, Baraja.NUEVE, 0);
                        } else {
                            baraja[p][n] = new Naipe(p, Baraja.CABALLO, 0);
                        }
                        break;
                    case SOTA:
                        if (con8y9 == true) {
                            baraja[p][n] = new Naipe(p, Baraja.SOTA, 0);
                        } else {
                            baraja[p][n] = new Naipe(p, Baraja.REY, 0);
                        }
                        break;
                    case CABALLO:
                        baraja[p][n] = new Naipe(p, Baraja.CABALLO, 0);
                        break;
                    case REY:
                        baraja[p][n] = new Naipe(p, Baraja.REY, 0);
                        break;
                }
            }
        }
    }

    public int getMazo() {
        return mazo;
    }

    public Naipe getNaipe(int palo, int numero) {
        return baraja[palo][numero - 1];
    }

    public boolean isCon8y9() {
        return con8y9;
    }
    
    public Naipe repartir() {
        if (mazo > 0) {
            Naipe n;
            int num, pal;
            do {
                if(con8y9){
                    num = (int) (Math.random() * 12) + 1;
                }else{
                    num = (int) (Math.random() * 10) + 1;
                }
                pal = (int) (Math.random() * 4);
                n = getNaipe(pal, num);
            } while (n.isRepartido() == true);
            n.setRepartido(true);
            mazo--;
            return n;
        }
        return null;//si mazo está vacío
    }

    @Override
    public String toString() {
        String b="";
        int nCartas;
        if(con8y9==true){
            b=b+"Baraja de 48 cartas\n";
            nCartas=48;
        }else{
            b=b+"Baraja de 40 cartas\n";
            nCartas=40;
        }
        b=b+"Cartas sin repartir="+mazo+"\n";
        Naipe n;
        for(int p=OROS;p<=BASTOS;p++){
            for(int num=1;num<=nCartas/4;num++){
                n=getNaipe(p, num);
                b=b+n+" valor="+n.getValor()+"\n";
            }
        }
        return b;
    }
    
    
}
